// create.component.js

import React, { Component } from 'react';
import axios from 'axios';
export default class Create extends Component {
  constructor(props){
    super(props);
    this.onChangeStudentName = this.onChangeStudentName.bind(this);
    this.onChangeCity=this.onChangeCity.bind(this);
    this.onChangeMobile=this.onChangeMobile.bind(this);
    this.onChangeEmail=this.onChangeEmail.bind(this);
    this.onSubmit=this.onSubmit.bind(this);
  

  this.state={
    student_name:'',
    city:'',
    mobile:'',
    email:'',
  };
}

  onChangeStudentName(e)
  {
this.setState({
  student_name: e.target.value
});
  }
  onChangeCity(e)
  {
this.setState({
  city: e.target.value
});
  }
  onChangeMobile(e)
  {
this.setState({
  mobile: e.target.value
});
  }
  onChangeEmail(e)
  {
this.setState({
  email: e.target.value
});
  }
  onSubmit(e) {
    e.preventDefault();
    const obj = {
      student_name: this.state.student_name,
      city: this.state.city,
      mobile: this.state.mobile,
      email: this.state.email
    };

axios.post('http://localhost:4000/student/add', obj)
        .then(res => console.log(res.data));
    
    this.setState({
      student_name: '',
      city: '',
      mobile: '',
      email:'',
    })
  }
    render() {
        return (
          <div style={{ marginTop: 10 }}>
          <h3>Add New student details</h3>
          <form onSubmit={this.onSubmit}>
              <div className="form-group">
                  <label>Student Name:  </label>
                  <input 
                    type="text" 
                    className="form-control" 
                    value={this.state.student_name}
                    onChange={this.onChangeStudentName}
                    />
              </div>
              <div className="form-group">
                  <label>City: </label>
                  <input type="text" 
                    className="form-control"
                    value={this.state.city}
                    onChange={this.onChangeCity}
                    />
              </div>
              <div className="form-group">
                  <label>Mobile Number: </label>
                  <input type="text" maxlength='10'
                    className="form-control"
                    value={this.state.mobile}
                    onChange={this.onChangeMobile}  />
              </div>
              <div className="form-group">
                  <label>Email Id: </label>
                  <input type="text" 
                    className="form-control"
                    value={this.state.email}
                    onChange={this.onChangeEmail}
                    />
              </div>
              <div className="form-group">
                  <input type="submit" value="Register" className="btn btn-success"/>
              </div>
          </form>
      </div>
        )
    }
}