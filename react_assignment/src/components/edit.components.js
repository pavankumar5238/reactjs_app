// edit.component.js

import React, { Component } from 'react';
import axios from 'axios';

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.onChangeStudentName = this.onChangeStudentName.bind(this);
    this.onChangeCity = this.onChangeCity.bind(this);
    this.onChangeMobile = this.onChangeMobile.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      student_name: '',
      city: '',
      mobile:'',
      email:''
    }
  }

  componentDidMount() {
      axios.get('http://localhost:4000/student/edit/'+this.props.match.params.id)
          .then(response => {
              this.setState({ 
                student_name: response.data.student_name, 
                city: response.data.city,
                mobile: response.data.mobile, 
                email:response.data.email });
          })
          .catch(function (error) {
              console.log(error);
          })
    }

  onChangeStudentName(e) {
    this.setState({
      student_name: e.target.value
    });
  }
  onChangeCity(e) {
    this.setState({
      city: e.target.value
    })  
  }
  onChangeMobile(e) {
    this.setState({
      mobile: e.target.value
    })
  }
  onChangeEmail(e){
    this.setState({
      email:e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      student_name: this.state.student_name,
      city: this.state.city,
      mobile: this.state.mobile,
      email: this.state.email
    };
    axios.post('http://localhost:4000/student/update/'+this.props.match.params.id, obj)
        .then(res => console.log(res.data));
    
    this.props.history.push('/index');
  }
 
  render() {
    return (
        <div style={{ marginTop: 10 }}>
            <h3 align="center">Update Student Details</h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Student Name:  </label>
                    <input 
                      type="text" 
                      className="form-control" 
                      value={this.state.student_name}
                      onChange={this.onChangeStudentName}
                      />
                </div>
                <div className="form-group">
                    <label>City Name: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.city}
                      onChange={this.onChangeCity}
                      />
                </div>
                <div className="form-group">
                    <label>Mobile Number: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.mobile}
                      onChange={this.onChangeMobile}
                      />
                </div>
                <div className="form-group">
                    <label>Email Id: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.email}
                      onChange={this.onChangeEmail}
                      />
                </div>
                <div className="form-group">
                    <input type="submit" 
                      value="Update" 
                      className="btn btn-primary"/>
                </div>
            </form>
        </div>
    )
  }
}