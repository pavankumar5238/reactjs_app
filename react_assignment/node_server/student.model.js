// student.model.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for student
let Student = new Schema({
  student_name: {
    type: String
  },
  city: {
    type: String
  },
  mobile: {
    type: Number
  },
  email:{
      type:String
  }
},{
    collection: 'student'
});

module.exports = mongoose.model('Student', Student);